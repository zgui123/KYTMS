package com.kytms.system.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Menu;
import com.kytms.system.dao.MenuDao;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 *
 * @author
 * @create 2017-11-18
 */
@Repository(value = "MenuDao")
public class MenuDaoImpl extends BaseDaoImpl<Menu> implements MenuDao<Menu> {
}
